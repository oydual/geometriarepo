#ifndef CUADRILATERO_H
#define CUADRILATERO_H

#include<punto.h>

class Cuadrilatero
{
protected:

    Punto _A, _B, _C, _D;
    const string TYPE = "Cuadrilatero";
    bool enCircunferencia();

public:
    Cuadrilatero();

    Cuadrilatero(Punto,Punto, Punto, Punto);
    Cuadrilatero(Cuadrilatero &);
    double getPerimetro();
    double getArea();
    void print(ostream & output);
    friend ostream & operator << ( ostream &, Cuadrilatero &);
};

#endif // CUADRILATERO_H
