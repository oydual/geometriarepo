#ifndef CUADRADO_H
#define CUADRADO_H
#include <rectangulo.h>
class Cuadrado: public Rectangulo
{
private:

    const string TYPE = "Cuadrilatero/Rectangulo/Cuadrado";
    void setCuadrado();

public:
    Cuadrado();
    Cuadrado( Punto, Punto, Punto, Punto);
    Cuadrado( Cuadrilatero &);
    double getPerimetro();
    double getArea();
    void print( ostream &);
    friend ostream & operator << ( ostream &, Cuadrado &);
};
#endif // CUADRADO_H
