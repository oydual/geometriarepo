#include <trapezoide.h>

void Trapezoide::setTrapezoide()
{

    double m_AB = _A.getPendiente(_B);
    double m_CD = _C.getPendiente(_D);
    double m_BC = _B.getPendiente(_C);
    double m_AD = _A.getPendiente(_D);
    if (!(m_AB == m_CD || m_BC == m_AD))
    {
       // _A = Punto(0,0);
       // _B = Punto(1,1);
       // _C = Punto(3,1);
       // _D = Punto(5,0);
       throw 1;
    }

    if (m_AB == m_CD)
    {
        if (_A.getDistancia( _B ) > _C.getDistancia( _D ))
        {
            _base1 = _A.getDistancia( _B );
            _base2 = _C.getDistancia( _D );
        }
        else
        {
            _base1 = _C.getDistancia( _D );
            _base2 = _A.getDistancia( _B );
        }
        altura = _B.getDistancia( _C );
    }
    else
    {
        if (_B.getDistancia( _C ) > _D.getDistancia( _A ))
        {
            _base1 = _B.getDistancia( _C );
            _base2 = _D.getDistancia( _A );
        }
        else
        {
            _base1 = _D.getDistancia( _A );
            _base2 = _B.getDistancia( _C );
        }
        altura = _A.getDistancia( _B );
    }
}

Trapezoide::Trapezoide(): Cuadrilatero()
{
    setTrapezoide();
}

Trapezoide::Trapezoide( Punto A, Punto B, Punto C, Punto D ):
Cuadrilatero( A, B, C, D )
{
    setTrapezoide();
}

Trapezoide::Trapezoide( Cuadrilatero & cuad ): Cuadrilatero( cuad )
{
    setTrapezoide();
}

Trapezoide::Trapezoide(Trapezoide & trap) : Cuadrilatero(_A,_B,_C,_D)
{
    _base1 = trap._base1;
    _base2 = trap._base2;
    altura = trap.altura;
}

double Trapezoide::getArea()
{
    return (( _base1 + _base2 ) * altura) / 2;
}

void Trapezoide::print( ostream & output =cout )
{
    output << "Propiedades Generales:" << endl;
    Cuadrilatero::print(output);
    output << "------------------------" << endl;
    output << TYPE << endl;
    output << "------------------------" << endl;
    output << "Propiedades Especificas:" << endl;
    output << "\tbase1= " << _base1 << endl;
    output << "\tbase2 = " << _base2 << endl;
    output << "\taltura = " << altura << endl;
    output << "\tPerimetro = " << getPerimetro() << endl;
    output << "\tArea = " << getArea() << endl << endl;
}

ostream & operator << ( ostream & output, Trapezoide & trap )
{
    trap.print( output );
    return output;
}
