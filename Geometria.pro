TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    punto.cpp \
    cuadrilatero.cpp \
    trapezoide.cpp \
    rectangulo.cpp \
    cuadrado.cpp

HEADERS += \
    punto.h \
    cuadrilatero.h \
    trapezoide.h \
    rectangulo.h \
    cuadrado.h
