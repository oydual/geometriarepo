#ifndef TRAPEZOIDE_H
#define TRAPEZOIDE_H

#include <cuadrilatero.h>
//-----------------------------------------------------------------------------
class Trapezoide: public Cuadrilatero
{
private:
    double _base1, _base2, altura;

    const string TYPE = "Cuadrilatero/Trapezoide";

    void setTrapezoide();

public:

    Trapezoide();
    Trapezoide( Punto, Punto, Punto, Punto );
    Trapezoide( Cuadrilatero &);
    Trapezoide( Trapezoide &);
    double getArea();
    void print( ostream &);
    friend ostream & operator << ( ostream &, Trapezoide & );
};
#endif // TRAPEZOIDE_H
