#include<rectangulo.h>

void Rectangulo::setRectangulo()
{
    double AB = _A.getDistancia( _B );
    double BC = _B.getDistancia( _C );
    double CD = _C.getDistancia( _D );
    double DA = _D.getDistancia( _A );

    bool altura = (AB == CD && BC == DA);

    double m_AB = _A.getPendiente( _B );
    double m_CD = _C.getPendiente( _D );
    double m_BC = _B.getPendiente( _C );
    double m_AD = _A.getPendiente( _D );
    bool pendiente = (m_AB == m_CD && m_BC == m_AD);

    if (!altura || !pendiente)
    {
       // A = Punto( 0, 0 );
       // B = Punto( 0, 5 );
       // C = Punto( 6, 5 );
       // D = Punto( 6, 0 );
       throw 1;
    }

    _altura = _A.getDistancia( _B );
    _base = _A.getDistancia( _D );
}

Rectangulo::Rectangulo(): Cuadrilatero()
{
    setRectangulo();
}

Rectangulo::Rectangulo( Punto A, Punto B, Punto C, Punto D ):Cuadrilatero ( A, B, C, D )
{
    setRectangulo();
}

Rectangulo::Rectangulo( Cuadrilatero & cuad ): Cuadrilatero( cuad )
{
    setRectangulo();
}

double Rectangulo::getArea()
{
    return _altura * _base;
}

double Rectangulo::getPerimetro()
{
    return (_altura + _base) * 2;
}

void Rectangulo::print( ostream & output  = cout)
{
    Cuadrilatero::print(output);
    output << "------------------------" << endl;
    output << TYPE << endl;
    output << "------------------------" << endl;
    output << "Propiedades Especificas:" << endl;
    output << "\tbase = " << _base << endl;
    output << "\taltura = " << _altura << endl;
    output << "\tPerimetro = " << getPerimetro() << endl;
    output << "\tArea = " << getArea() << endl << endl;
}

ostream & operator << ( ostream & output, Rectangulo & rect )
{
    rect.print( output );
    return output;
}

