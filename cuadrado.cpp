#include "cuadrado.h"

void Cuadrado::setCuadrado()
{
    Rectangulo::setRectangulo();

    if (_base != _altura)
    {
        //_A = Punto( 0, 0 );
        //_B = Punto( 0, 5 );
        //_C = Punto( 5, 5 );
        //_D = Punto( 5, 0 );
        throw 1;
    }
}

Cuadrado::Cuadrado(): Rectangulo()
{
    setCuadrado();
}

Cuadrado::Cuadrado( Punto A, Punto B, Punto C, Punto D ):
Rectangulo( A, B, C, D )
{
    setCuadrado();
}

Cuadrado::Cuadrado( Cuadrilatero & cuad ): Rectangulo( cuad )
{
    setCuadrado();
}
double Cuadrado::getPerimetro()
{
    return _base * 4;
}

double Cuadrado::getArea()
{
    return _altura * _altura;
}

void Cuadrado::print( ostream & output  = cout)
{
    Cuadrilatero::print(output);
    cout << "--------------------------------" << endl;
    output << TYPE << endl;
    cout << "--------------------------------" << endl;
    output << "Propiedades Especificas:" << endl;
    output << "\tlado = " << _base << endl;
    output << "\tPerimetro = " << getPerimetro() << endl;
    output << "\tArea = " << getArea() << endl << endl;
}

ostream & operator << ( ostream & output, Cuadrado & cua )
{
    cua.print( output );
    return output;
}
