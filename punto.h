#ifndef PUNTO_H
#define PUNTO_H

#include <iostream>
#include <cmath>
#include <string>
using namespace std;


class Punto
{
private:

    double _x,_y;

public:

    Punto();
    Punto(double,double);
    Punto(Punto&);
    void setX(double);
    void setY(double);
    double getX() const;
    double getY() const;
    double getDistancia(Punto&);
    double getPendiente(Punto&);
    void print(ostream&);
    void operator=(const Punto &);
    friend ostream & operator << ( ostream &, Punto &);

};

#endif // PUNTO_H
