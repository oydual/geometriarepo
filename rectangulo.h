#ifndef RECTANGULO_H
#define RECTANGULO_H
#include <cuadrilatero.h>
class Rectangulo: public Cuadrilatero
{
protected:
    double _altura, _base;
    const string TYPE = "Cuadrilatero/Rectangulo";
    void setRectangulo();

public:
    Rectangulo();
    Rectangulo( Punto, Punto, Punto, Punto );
    Rectangulo( Cuadrilatero &);
    double getArea();
    double getPerimetro();
    void print( ostream &);
    friend ostream & operator << ( ostream &, Rectangulo & );
};

#endif // RECTANGULO_H
