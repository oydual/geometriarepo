#include "cuadrilatero.h"


bool Cuadrilatero::enCircunferencia()
{
    //Teorema de Ptolemy
    double AB = _A.getDistancia( _B );
    double BC = _B.getDistancia( _C );
    double CD = _C.getDistancia( _D );
    double DA = _D.getDistancia( _A );
    double AC = _A.getDistancia( _C );
    double BD = _B.getDistancia( _D );
    if ( (AB * CD * BC * DA) == (AC * BD) ) return true;
    return false;
}

Cuadrilatero::Cuadrilatero()
{
    _A = Punto( 0, 0 );
    _B = Punto( 1, 1 );
    _C = Punto( 2, 1 );
    _D = Punto( 3, -1 );
}

Cuadrilatero::Cuadrilatero( Punto A, Punto B, Punto C, Punto D )
{
    try
    {
        if ( enCircunferencia())
        {
            _A = Punto(A);
            _B = Punto(B);
            _C = Punto(C);
            _D = Punto(D);
        }
        else
        {
            throw 1;
        }
    }
    catch(...)
    {
        cout << "Eror de Definicion de Cuadrilatero: No es ciclico" << endl;
    }

}

Cuadrilatero::Cuadrilatero( Cuadrilatero & C )
{
    _A = C._A;
    _B = C._B;
    _C = C._C;
    _D = C._D;
}

double Cuadrilatero::getPerimetro()
{
    return ( _A.getDistancia( _B ) + _B.getDistancia( _C ) +
             _C.getDistancia( _D ) + _D.getDistancia( _A ) );
}

double Cuadrilatero::getArea()
{
    //Formula de Heron
    double AB = _A.getDistancia( _B );
    double BC = _B.getDistancia( _C );
    double CA = _C.getDistancia( _A );

    double AC = _A.getDistancia( _C );
    double CD = _C.getDistancia( _D );
    double DA = _D.getDistancia( _A );

    double S1 = ( AB + BC + CA ) / 2;
    double S2 = ( AC + CD + DA ) / 2;

    double A1 = sqrt( S1*( S1-AB )*( S1-BC )*( S1-CA ));
    double A2 = sqrt( S2*( S2-AC )*( S2-CD )*( S2-DA ));

    return A1 + A2;
}

void Cuadrilatero::print( ostream & output = cout)
{
    output << "--------------" << endl;
    output << TYPE << endl;
    output << "--------------" << endl;
    output << "Puntos:" << endl;
    output << "\tA = " << _A << endl;
    output << "\tB = " << _B << endl;
    output << "\tC = " << _C << endl;
    output << "\tD = " << _D << endl;
    output << "Largo de los Lados:n";
    output << "\tAB = " << _A.getDistancia( _B ) << endl;
    output << "\tBC = " << _B.getDistancia( _C ) << endl;
    output << "\tCD = " << _C.getDistancia( _D ) << endl;
    output << "\tDA = " << _D.getDistancia( _A ) << endl;
    output << "Shape Properties:" << endl;
    output << "\tPerimeter = " << getPerimetro() << endl;
    output << "\tArea = " << getArea() << endl << endl;
}

ostream & operator << ( ostream & output, Cuadrilatero & quad )
{
    quad.print( output );
    return output;
}
