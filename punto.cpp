#include "punto.h"

Punto::Punto(): _x(0.0f) , _y(0.0f)
{
}
Punto::Punto( double x, double y ) : _x(x),_y(y)
{
}

Punto::Punto( Punto & p ) : _x(p._x) , _y(p._y)
{
}

void Punto::setX( double x )
{
    _x = x;
}

void Punto::setY( double y )
{
    _y = y;
}

double Punto::getX() const
{
    return _x;
}

double Punto::getY() const
{
    return _y;
}

double Punto::getDistancia( Punto & p)
{
    return sqrt( pow(_x - p._x, 2) + pow(_y - p._y, 2));
}

double Punto::getPendiente(Punto & p)
{
    try
    {
        if (_x == p._x)
        {
            throw 1;
        }

    }
    catch(...)
    {
          cout << "Error matematico : Division por cero" << endl;
    }
    return (_y - p._y)/(_x - p._x);
}


void Punto::print( ostream & output = cout)
{
    output << "(" << _x << "," << _y << ")";
}
void Punto::operator =(const Punto &p)
{
   _x = p._x;
   _y = p._y;
}
ostream & operator << ( ostream & output, Punto & p )
{
    p.print( output );
    return output;
}
